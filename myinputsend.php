<html>
<body>
<?php header('Access-Control-Allow-Origin: *'); ?>
<?php 


/**
 * The following function will send a GCM notification using curl.
 * 
 * @param $apiKey		[string] The Browser API key string for your GCM account
 * @param $registrationIdsArray [array]  An array of registration ids to send this notification to
 * @param $messageData		[array]	 An named array of data to send as the notification payload
 */
function sendNotification( $apiKey, $registrationIdsArray, $messageData )
{   
    $headers = array("Content-Type:" . "application/json", "Authorization:" . "key=" . $apiKey);
    $data = array(
        'data' => $messageData,
        'registration_ids' => $registrationIdsArray
    );
 
    $ch = curl_init();
 
    curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers ); 
    curl_setopt( $ch, CURLOPT_URL, "https://android.googleapis.com/gcm/send" );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
    curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
    curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
    curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($data) );
 
    $response = curl_exec($ch);
    curl_close($ch);
 
    return $response;
}

// Message to send
$message      = "the test message";
 
$registrationId = 'APA91bE21O2bPqOev2W8OdyurHCweqL96ITG5CPmF1YtuMDMHv35NKvW7Zg3Hec3wvgcRKXuw-yXyE9-xnruEl9Q5Zd5_4RACXBX4XuUYsaNyOjLJhz5I4aM3lGOnIwYLRfDNf_hCUVZ-AS1icyG5fXlXM_rdZfThAdhJV3jCUCsuGRxBbBZZhg';
$apiKey = "AIzaSyCxz5g1vpyuaDLu7SAOeVS0ntCr9YrElOc";


if(isset($_POST['message']))
{
    $message = $_POST['message'];
	echo "</br>";
} else {
   echo "NO message parameter </br>";
}

if(isset($_POST['regid']))
{
    $registrationId = $_POST['regid'];
	echo "</br>";
} else {
   echo "NO regid parameter </br>";
}

if(isset($_POST['apikey']))
{
    $apiKey = $_POST['apikey'];
	echo "</br>";
} else {
   echo "NO regid parameter </br>";
}

 


?> 

Your message: <?php echo $_POST["message"]; ?><br />
<?php
 $response = sendNotification( 
                $apiKey, 
                array($registrationId), 
                array('message' => $message) );
 
echo $response;
?>
</body>
</html>